import pytest
import sqlalchemy
from sqlalchemy.orm import sessionmaker, Session
from testcontainers.postgres import PostgresContainer

from src.features.erp.adapters.secondaries.pg_erp_repository import PgErpRepository
from src.features.erp.core.domain.erp import Erp
from src.shared.database.models import ErpModel, Base


@pytest.fixture
def session():
    with PostgresContainer('postgis/postgis:12-master') as pg:
        engine = sqlalchemy.create_engine(pg.get_connection_url())
        session_fact = sessionmaker(bind=engine)
        session: Session = session_fact()
        Base.metadata.create_all(bind=session.get_bind())
        yield session

        session.close()


@pytest.mark.asyncio
async def test_add_erp(session):
    repository = PgErpRepository(session)
    erp = Erp.create("test", "42227627900011")
    
    await repository.add(erp)
    session.commit()

    res = session.query(ErpModel).all()
    assert res[0].name == erp.name
    assert res[0].siret == erp.siret.number


def test_edit_erp():
    pass
