from fastapi.testclient import TestClient
from fastapi.responses import Response

from main import app

client = TestClient(app=app)


def test_list_erp():
    response: Response = client.get("/erp")
    assert response.status_code == 200
    # assert response.json() == {}
