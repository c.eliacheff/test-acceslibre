from typing import List

import pytest

from src.features.erp.adapters.primaries.dtos import ErpIn
from src.features.erp.adapters.secondaries.fake_erp_repository import InMemoryErpRepository
from src.features.erp.adapters.secondaries.fake_erp_views import InMemoryErpViews
from src.features.erp.core.domain.erp import Erp
from src.features.erp.core.usecases.add_erp import AddErp
from src.features.erp.core.usecases.list_erps import ListErps


@pytest.fixture
def init_data():
    erp1 = Erp.create(name="test1", siret="73282932000074")
    erp2 = Erp.create(name="test2", siret="39263695700046")
    erp3 = Erp.create(name="test3", siret="42227627900011")

    return [erp1, erp2, erp3]


@pytest.fixture
def dependencies():
    deps = {
        "repository": InMemoryErpRepository(),
        "view": InMemoryErpViews()
    }

    return deps


@pytest.mark.asyncio
async def test_list_erps_if_no_erps(dependencies):
    view = dependencies['view']
    list_erp_handler = ListErps(view)

    results = await list_erp_handler.handle()

    assert results == []


@pytest.mark.asyncio
async def test_list_erps(dependencies, init_data):
    view = dependencies['view']
    view.feed_with(init_data)
    list_erp_handler = ListErps(view)

    results: List[Erp] = await list_erp_handler.handle()

    assert results == init_data


@pytest.mark.asyncio
async def test_add_valid_erp(dependencies):
    repository = dependencies['repository']
    add_erp_handler = AddErp(repository)
    new_valid_erp = ErpIn.parse_obj({"name": "test1", "siret": "42227627900011"})

    await add_erp_handler.handle(new_valid_erp)

    assert len(repository.erps) == 1
    assert repository.erps[0].name == "test1"
    assert repository.erps[0].siret.is_valid() is True
    assert repository.erps[0].siret.number == "42227627900011"
