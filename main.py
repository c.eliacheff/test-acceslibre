from dotenv import load_dotenv
from fastapi import FastAPI

from src.features.erp.adapters.primaries.routers import router as erp_router

load_dotenv()

app = FastAPI()


@app.get("/")
async def read_root():
    return {"Hello": "World"}

app.include_router(erp_router)
