import os
import sys

sys.path.append("../../../src")

# All models must be imported so the appropriate tables are created


def init_db(env=None):
    try:
        env = os.getenv('ENV') or env

        print("---------")
        print(">Database successfully initialized")
        print("---------")

    except Exception as e:
        print("---------")
        print(e)
        print("---------")


if __name__ == "__main__":
    init_db()
