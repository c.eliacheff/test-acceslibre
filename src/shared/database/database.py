import os

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from src.shared.database.models import Base

engine = create_engine(os.getenv('DATABASE_URL'), echo=True, future=True)
Session = sessionmaker(engine)
session = Session()

Base.metadata.create_all(bind=session.get_bind())
