from typing import List

from src.features.erp.core.domain.erp import Erp
from src.features.erp.core.gateways.erp_repository import ErpRepository


class InMemoryErpRepository(ErpRepository):
    erps = []

    def feed_with(self, erps: List[Erp]):
        self.erps = erps
        return self

    async def add(self, erp: Erp) -> None:
        self.erps.append(erp)
