from typing import List, Any

from src.features.erp.core.domain.erp import Erp
from src.features.erp.core.gateways.erp_views import ErpViews


class InMemoryErpViews(ErpViews):
    erps = []

    def feed_with(self, erps: List[Erp]):
        self.erps = erps
        return self

    async def all(self) -> Any:
        return self.erps
