from typing import List

from sqlalchemy.orm import Session

from src.features.erp.core.domain.erp import Erp
from src.features.erp.core.gateways.erp_repository import ErpRepository
from src.shared.database.models import ErpModel


class PgErpRepository(ErpRepository):
    def __init__(self, session: Session):
        self.session = session

    async def add(self, erp: Erp) -> None:
        new_erp = ErpModel(**erp.get_state())
        self.session.add(new_erp)
