from typing import List

from fastapi import APIRouter, Depends

from src.features.erp.adapters.primaries.dtos import ErpOut, ErpIn
from src.features.erp.core.usecases.add_erp import AddErp
from src.features.erp.core.usecases.list_erps import ListErps
from src.features.erp.dependencies import ErpDependencies

router = APIRouter(
    prefix="/erp",
    tags=["erps"],
)


@router.get("/", response_model=List[ErpOut])
async def list_erps(deps: ErpDependencies = Depends()):
    return await ListErps(deps.views).handle()


@router.post("/")
async def add_erp(erp: ErpIn, deps: ErpDependencies = Depends()):
    await AddErp(deps.repository).handle(erp)
