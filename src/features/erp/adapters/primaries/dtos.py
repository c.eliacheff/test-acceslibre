from pydantic import BaseModel


class ErpOut(BaseModel):
    name: str
    siret: str


class ErpIn(BaseModel):
    name: str
    siret: str
