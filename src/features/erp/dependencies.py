from src.features.erp.adapters.secondaries.fake_erp_repository import InMemoryErpRepository
from src.features.erp.adapters.secondaries.fake_erp_views import InMemoryErpViews
from src.features.erp.core.gateways.erp_repository import ErpRepository
from src.features.erp.core.gateways.erp_views import ErpViews


class ErpDependencies:
    repository: ErpRepository
    views: ErpViews

    def __init__(self):
        self.repository = InMemoryErpRepository()
        self.views = InMemoryErpViews()
