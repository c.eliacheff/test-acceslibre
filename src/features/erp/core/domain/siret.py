from dataclasses import dataclass

from src.features.erp.core.errors import InvalideSiretError


@dataclass(frozen=True)
class Siret:
    number: str

    def __post_init__(self):
        if not self.is_valid():
            raise InvalideSiretError()

    def is_valid(self) -> bool:
        if len(self.number) != 14:
            return False

        if not self.luhn_checksum(self.number):
            return False

        return True

    # https://www.rosettacode.org/wiki/Luhn_test_of_credit_card_numbers#Python
    @staticmethod
    def luhn_checksum(value: str) -> bool:
        r = [int(ch) for ch in value][::-1]
        return (sum(r[0::2]) + sum(sum(divmod(d * 2, 10)) for d in r[1::2])) % 10 == 0
