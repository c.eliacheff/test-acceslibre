from dataclasses import dataclass, asdict
from typing import TypedDict

from src.features.erp.core.domain.siret import Siret

ErpMemento = TypedDict('ErpMemento', {
    'name': str,
    'siret': str
})


class Erp:
    siret: Siret
    name: str

    def __init__(self, name: str, siret: Siret):
        self.name = name
        self.siret = siret

    @staticmethod
    def create(name: str, siret: str) -> "Erp":
        valid_siret = Siret(siret)
        return Erp(name=name, siret=valid_siret)

    def get_state(self) -> ErpMemento:
        # return asdict(self)
        return {
            'name': self.name,
            'siret': self.siret.number
        }

    @staticmethod
    def from_state(values: ErpMemento) -> "Erp":
        return Erp.create(**values)


# erp = Erp.from_state({'name': 'toto', 'siret': '42227627900011'})
# print(erp.name)
