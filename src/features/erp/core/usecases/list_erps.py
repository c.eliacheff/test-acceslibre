from typing import List, Any

from src.features.erp.core.gateways.erp_views import ErpViews


class ListErps:
    def __init__(self, views: ErpViews):
        self.views = views

    async def handle(self) -> List[Any]:
        return await self.views.all()
