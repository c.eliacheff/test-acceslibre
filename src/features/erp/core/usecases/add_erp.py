from src.features.erp.adapters.primaries.dtos import ErpIn
from src.features.erp.core.domain.erp import Erp
from src.features.erp.core.gateways.erp_repository import ErpRepository


class AddErp:
    def __init__(self, repository: ErpRepository):
        self.repository = repository

    async def handle(self, dto: ErpIn) -> None:
        erp = Erp.create(dto.name, dto.siret)
        await self.repository.add(erp)
