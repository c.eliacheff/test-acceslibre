import abc


class ErpViews(abc.ABC):
    @abc.abstractmethod
    async def all(self):
        pass
