import abc
from typing import List

from src.features.erp.core.domain.erp import Erp


class ErpRepository(abc.ABC):
    @abc.abstractmethod
    async def add(self, erp: Erp) -> None:
        pass
